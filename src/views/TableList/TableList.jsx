import React from 'react';
// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
// core components
import GridItem from 'components/Grid/GridItem.jsx';
import Card from 'components/Card/Card.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const tableHead = ['Content', 'Reply', 'Time', 'Image', 'Action'];
class TableList extends React.Component {
  // static getDerivedStateFromProps(nextProps, prevState){
  // }
  state = {
    modalVisible: false,
    data: []
  };

  async componentDidMount() {
    let data = await axios.get('http://uetcfs.cf/api/confessions');
    this.setState({ data: data.data.data });
  }

  closeModal = () => this.setState({ modalVisible: false });

  render() {
    const { classes } = this.props;
    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Confessions</h4>
            </CardHeader>
            <CardBody>
              {/* <Table
                tableHeaderColor="primary"
                tableHead={['Content', 'Reply', 'Time', 'Image', 'Action']}
                tableData={this.state.data}
              /> */}
              <div className={classes.tableResponsive}>
                <Table className={classes.table}>
                  {tableHead !== undefined ? (
                    <TableHead className={classes['primaryTableHeader']}>
                      <TableRow>
                        {tableHead.map((prop, key) => {
                          return (
                            <TableCell
                              className={
                                classes.tableCell + ' ' + classes.tableHeadCell
                              }
                              key={key}
                            >
                              {prop}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    </TableHead>
                  ) : null}
                  <TableBody>
                    {this.state.data.map((confession, key) => {
                      return (
                        <TableRow key={key}>
                          <TableCell className={classes.tableCell}>
                            {confession.content}
                          </TableCell>
                          <TableCell className={classes.tableCell}>
                            {confession.reply}
                          </TableCell>
                          <TableCell className={classes.tableCell}>
                            {confession.time}
                          </TableCell>
                          <TableCell className={classes.tableCell}>
                            {confession.image}
                          </TableCell>
                          <TableCell>
                            <Button
                              variant="contained"
                              color="primary"
                              className={classes.button}
                              onClick={() =>
                                this.setState({ modalVisible: true })
                              }
                            >
                              Primary
                            </Button>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </div>
            </CardBody>
          </Card>
        </GridItem>
        <Modal isOpen={this.state.modalVisible} toggle={this.closeModal}>
          <ModalHeader toggle={this.closeModal}>Modal title</ModalHeader>
          <ModalBody>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.setState({ modalVisible: false })}
            >
              Do Something
            </Button>
          </ModalFooter>
        </Modal>
      </Grid>
    );
  }

  getModalStyle = () => {
    const top = 50; //window.innerHeight / 2;
    const left = 50;

    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`
    };
  };
}

const styles = {
  cardCategoryWhite: {
    '&,& a,& a:hover,& a:focus': {
      color: 'rgba(255,255,255,.62)',
      margin: '0',
      fontSize: '14px',
      marginTop: '0',
      marginBottom: '0'
    },
    '& a,& a:hover,& a:focus': {
      color: '#FFFFFF'
    }
  },
  cardTitleWhite: {
    color: '#FFFFFF',
    marginTop: '0px',
    minHeight: 'auto',
    fontWeight: '300',
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: '3px',
    textDecoration: 'none',
    '& small': {
      color: '#777',
      fontSize: '65%',
      fontWeight: '400',
      lineHeight: '1'
    }
  }
};

export default withStyles(styles)(TableList);
